#!/usr/bin/env python

"""

Bare bones flavor tagging info dumper

You should consider this the starting point for any more complicated
studies, all it does is set up the basic event loop and schedule the
dataset dumper.

"""

import sys
import json

from AthenaConfiguration.ComponentFactory import CompFactory

from FTagDumper import dumper


def run():

    args = dumper.base_parser(__doc__).parse_args()

    flags = dumper.update_flags(args)
    flags.lock()

    ca = dumper.getMainConfig(flags)

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    with open(args.config_file) as f:
        config_keys = json.load(f).keys()

    for cfg in config_keys:
        dumper_cfg = dumper.combinedConfig(args.config_file)[cfg]
        dumper_cfg = dumper_cfg.get("dumper", dumper_cfg)
        if "ca_blocks" in dumper_cfg:
            dumper_cfg.pop("ca_blocks")
        ca.addEventAlgo(CompFactory.JetDumperAlg(
            name=f'{cfg}Dumper',
            output=output,
            configJson=json.dumps(dumper_cfg),
            group=cfg,
        ))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
