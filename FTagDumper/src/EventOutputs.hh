#ifndef EVENT_OUTPUTS_HH
#define EVENT_OUTPUTS_HH

struct EventOutputs {
  const xAOD::EventInfo* event_info;
  size_t first_jet_index;
  size_t next_jet_index;
};

#endif
